<?php 
$activeLinkName = 'blogList';
$x = 0;
$headerBackgroundBottomForOthersPages = true;
include './get-data.php';
include './header.php';
$templateData = $templateData['home']; 
include './header/header.php';
$footerIndex = count($templateData) - 1;
include './commonFunctions/functions.php';
//$userId = htmlspecialchars($_GET["userId"]);
$data = $database->getReference('user_blog_data/'.$userId)->getSnapshot()->getValue();
//echo json_encode($data);
if($data){
  $dataValues = array_values($data);  
  $dataKeys = array_keys($data);
} else {
  $dataValues = null;
  $dataKeys = null;  
}



if ($userId == undefined || $userId == null || $data == null) {
	$showData = false;
	
} else {
	$showData = true;
	
}
?>

<div class="container" style="margin-top: 150px">
  <div class="table-responsive">
  <table class="table">
    <thead>
    <tr>
      <th>#</th>
        <th>Title</th>
        <th>Author</th>
        <th>Category</th>
        <th>Date</th>
        <th></th>
        </tr>
    </thead>
    <tbody>
      <?php
  if($showData){
  $count = 1;
  foreach ($dataValues as $value) {
  if($value['status'] == 'active'){
    echo "<tr>
          <td>".$count."</td>
          <td style='width: 180px;'>".$value['title']."</td>
          <td style='max-height: 40px;'>".$value['authorName']."</td>
          <td>".$value['category']."</td>
          <td>".$value['date']."</td>
          <td style='width: 150px;'><a href='blog/".$dataKeys[$count-1]."'>Read</a></td>
        </tr>  ";

$count++;
}
}
} else {
  echo "<h1>No Blog Found</h1>";
}
?>
    </tbody>
  </table>
</div>
</div>

<?php 

include './footer/footer.php'; 
?>
   
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Additional Scripts -->
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>