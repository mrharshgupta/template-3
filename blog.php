<?php 
$activeLinkName = 'blog';
$id = htmlspecialchars($_GET["id"]);
$x = 0;
$headerBackgroundBottomForOthersPages = true;
$includeBaseTag = true;
include './get-data.php';
include './header.php';
$templateData = $templateData['home'];
include './header/header.php';
 

//$userId = htmlspecialchars($_GET["userId"]);
$data = $database->getReference('user_blog_data/'.$userId.'/'.$id)->getSnapshot()->getValue();
//echo json_encode($data);
if ($id == undefined || $id == null || $data == null) {
    $showData = false;
} else {
    $showData = true;
}
include './commonFunctions/functions.php';
$footerIndex = count($templateData) - 1;

?>
<div style="width: 100%;height: auto;margin-top: 150px;">
   <div style='width: 100%; height: 50px; display: flex;
  align-items: center;
  justify-content: center'>
    <center><h5><?php echo $data['title']; echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data['date']; ?></h5></center>
   </div>
  <div style="margin-left: 15px; margin-right: 15px; margin-top: 20px">
    <?php
      echo $data['description'];
     ?> 
     <hr>
  </div>
  <center>
  <?php
  if($data['blogImage']){
  echo '<img src='.$data['blogImage'].' style="height: 200px; width: 200px; object-fit: contain">';
  }
  ?>
  </center>
  <div style="margin-left: 15px; margin-right: 15px;">
    <?php
      echo $data['content'];
     ?> 
  </div>

  
    </div>

    <?php include './footer/footer.php'; ?>
   
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Additional Scripts -->
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>