<?php
if($activeComponent == 'carousel'.$x){
$showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}

$carouselButtons = ['contact', 'services', 'about'];
if ($dataToShow == 'templateStockData') {
  echo '<div class="main-banner header-text" id="top" style="'.$showBorder.'">';
echo '<div class="Modern-Slider" id="carousel'.$x.'">';
  echo '<div class="item item-1">
            <div class="img-fill">
                <div class="text-content">
                  <h6>we are ready to help you</h6>
                  <h4>Financial Analysis<br>&amp; Consulting</h4>
                  <p>This finance HTML template is 100% free of charge provided by TemplateMo for everyone. You can download, edit and use this layout for your business website.</p>
                  <a href="contact.html" class="filled-button">contact us</a>
                </div>
            </div>
          </div>

          <div class="item item-2">
            <div class="img-fill">
                <div class="text-content">
                  <h6>we are here to support you</h6>
                  <h4>Accounting<br>&amp; Management</h4>
                  <p>You are allowed to use this template for your company websites. You are NOT allowed to re-distribute this template ZIP file on any template download website. Please contact TemplateMo for more detail.</p>
                  <a href="services.html" class="filled-button">our services</a>
                </div>
            </div>
          </div>

          <div class="item item-3">
            <div class="img-fill">
                <div class="text-content">
                  <h6>we have a solid background</h6>
                  <h4>Market Analysis<br>&amp; Statistics</h4>
                  <p>Vivamus ut tellus mi. Nulla nec cursus elit, id vulputate mi. Sed nec cursus augue. Phasellus lacinia ac sapien vitae dapibus. Mauris ut dapibus velit cras interdum nisl ac urna tempor mollis.</p>
                  <a href="about.html" class="filled-button">learn more</a>
                </div>
            </div>
          </div>';
          echo '</div>
    </div>';
} else {
  if($templateData[$x]['sectionSettings']['show'] == true){
    
   echo '<div class="main-banner header-text" id="top" style="'.$showBorder.'">';
echo '<div class="Modern-Slider" id="carousel'.$x.'">';
$count = count($templateData[$x]['data']['slides']);
if($templateData[$x]['data']['slides'][0] != 'true'){
  for ($i = 0; $i < $count; $i++) {
    $link = getBlogLink($templateData[$x]['data']['slides'][$i]['button']['linkType'], $templateData[$x]['data']['slides'][$i]['button']['link'], $parameters);
   $itemIndex = $i+1;
    echo "<div class='item item-".$itemIndex."'>";//
     if($templateData[$x]['data']['slides'][$i]['backgroundImage'] != ''){    
      $rand = rand();
       echo"<div class='img-fill' style='background-image: url(".$imagesDirectory."/".$templateData[$x]['data']['slides'][$i]['backgroundImage']."?rand=".$rand.");'>";
     } else {
      echo"<div class='img-fill'>";
    }
    echo"<div class='text-content'>
        <h6>".$templateData[$x]['data']['slides'][$i]['heading']."</h6>
        <h4>".$templateData[$x]['data']['slides'][$i]['title']."</h4>
        <p>".$templateData[$x]['data']['slides'][$i]['description']."</p>";
      
if($templateData[$x]['data']['slides'][$i]['button']['styles']['backgroundColor']){
                $buttonColor = $templateData[$x]['data']['slides'][$i]['button']['styles']['backgroundColor'];
              } else {
                $buttonColor = null;
              }  

      if($templateData[$x]['data']['slides'][$i]['button']['targetBlank']){
        echo "<a href=".$link." class='filled-button' target='_blank' style='background-color: ".$buttonColor.";'>";
      } else {
        echo "<a href=".$link." class='filled-button' style='background-color: ".$buttonColor.";'>";
      }
        echo $templateData[$x]['data']['slides'][$i]['button']['text']."</a>
      </div>
  </div>
</div>";

}}
echo '</div>
    </div>';
}}?>
