<?php

function getBlogLink($linkType, $customLink, $parameters){
  
    $linkArray = explode("-",$linkType);

    if($linkType == 'default'){
        $link = 'javascript:void(0)';
      } else if($linkType == 'blog') {
        //$link = $blogLink;
        $link = 'blog-list'.$parameters;
      } else if ($linkType == 'other'){
        $link = $customLink;
      } else if($linkType == 'about' || $linkType == 'contact' || $linkType == 'service'){
        $link = $linkType;
      } else if($linkType == 'home'){
        $link = 'index';
      } else if($linkArray[0] == 'blog'){
        $parameters ? $link = 'blog'.$parameters.'&id='.$linkType : $link = 'blog?id='.$linkType;
      } else if($linkArray[0] == 'webpage') {
        //$link = $templateData[$x]['data']['button']['linkType'].$parameters;
        //$link = 'webpage'.$parameters.'&id='.$linkType;
        $parameters ? $link = 'webpage'.$parameters.'&id='.$linkType : $link = 'webpage?id='.$linkType;
      }
      return $link;
      
}

?>