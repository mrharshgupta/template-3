<?php
$activeLinkName = 'contact';
$x = 0;
include 'header.php';
$completeTemplateData = $templateData;
$templateData = $templateData['home'];
include './header/header.php';
$templateData = $completeTemplateData['contact'];
include './commonFunctions/functions.php';

function includeFile($file, 
                     $templateData, 
                     $dataToShow, 
                     $parameter, 
                     $borderStyle, 
                     $imagesDirectory, 
                     $userId, 
                     $activeComponent, 
                     $blogLink, 
                     $x,
                     $storeInformation) {
    $templateData = $templateData;
    $dataToShow = $dataToShow; 
    $parameters = $parameters; 
    $borderStyle = $borderStyle; 
    $imagesDirectory = $imagesDirectory; 
    $userId = $userId; 
    $activeComponent = $activeComponent; 
    $blogLink = $blogLink;
    $storeInformation = $storeInformation;

    include($file);
}

if(!$maintenanceMode){
  
  if($dataToShow == 'templateStockData'){
   include './textWithBackgroundImage/textWithBackgroundImage.php';
   
  include './contactForm/contactForm.php'; 
   include './partners/partners.php';
  
  } else {
   
   
  $count = count($templateData);
  
   for($x = 0; $x < $count; $x++ ){
   
    if($templateData[$x]['sectionSettings']['show'] == 'true'){
      includeFile('./'.$templateData[$x]['sectionInfo']['type'].'/'.$templateData[$x]['sectionInfo']['type'].'.php', 
                  $templateData, 
                  $dataToShow, 
                  $parameters, 
                  $borderStyle, 
                  $imagesDirectory, 
                  $userId, 
                  $activeComponent, 
                  $blogLink,
                  $x,
                  $storeInformation
                );
    }
   }
  }
  } else {
  include './maintenance.html';
  // echo '<h1>Under Maintenance</h1>';
  }
  
  if(!$maintenanceMode){
    $templateData = $completeTemplateData['home'];
    $x = count($templateData) - 1;
    include './footer/footer.php';
  }

?>
    
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Additional Scripts -->
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/accordions.js"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>