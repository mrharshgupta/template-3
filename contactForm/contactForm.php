<?php

if ($dataToShow == 'templateStockData') {
  $title = 'Request a <em>call back</em>';
  $description = 'Etiam suscipit ante a odio consequat';
  
} else{
  $title = $templateData[$x]['data']['title']['text'];
  $description = $templateData[$x]['data']['description']['text'];
  
}
if($activeComponent == 'contactForm'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}
if($templateData[$x]['data']['styles']['backgroundColor']){
  $backgroundColor = $templateData[$x]['data']['styles']['backgroundColor'];
} else {
  $backgroundColor = '#a4c639';
}
?>

<div class="callback-form" id="contactForm<?php echo $x; ?>" style="<?php echo $showBorder ?>">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2><?php echo $title; ?></h2>
              <span><?php echo $description; ?></span>
            </div>
          </div>
          <div class="col-md-12">
            <div class="contact-form" style="background-color: <?php echo $backgroundColor; ?>">
              <form id="contact" action="" method="post">
                <div class="row">
                  <div class="col-lg-4 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="name" type="text" class="form-control" id="name" placeholder="Full Name" required="">
                    </fieldset>
                  </div>
                  <div class="col-lg-4 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="email" type="text" class="form-control" id="email" pattern="[^ @]*@[^ @]*" placeholder="E-Mail Address" required="">
                    </fieldset>
                  </div>
                  
                  <?php 
                  if($storeInformation['showClientPhoneNumberField']){
                  echo'<div class="col-lg-4 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="phone" type="text" class="form-control" id="subject" placeholder="Phone Number" required>
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <input name="subject" type="text" class="form-control" id="subject" placeholder="Subject" required="">
                    </fieldset>
                  </div>';
                  } else {
                    echo'<div class="col-lg-4 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="subject" type="text" class="form-control" id="subject" placeholder="Subject" required="">
                    </fieldset>
                  </div>';
                  } ?>
                  
                  <div class="col-lg-12">
                    <fieldset>
                      <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <button type="submit" name="submit" id="form-submit" class="border-button">Send Message</button>
                    </fieldset>
                  </div>
                  <?php
          if(isset($_POST['submit'])){
            $name=$_POST['name'];
            $email=$_POST['email'];
            $title=$_POST['subject'];
            $message=$_POST['message'];
            $phone=$_POST['phone'];
                
if(empty($storeInformation["email"])){
  $to = $templateData[$x]['data']['email'];
} else {
  $to = $storeInformation["email"];
}
            
//$to = 'harshgupta.harry@gmail.com';
$subject = $title;
$txt = "Full Name: ".$name." Email: ".$email." Phone: ".$phone." message: ".$message;
$headers = "From: cling@example.com" . "\r\n";

mail($to,$subject,$txt,$headers);

          }
          ?>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
