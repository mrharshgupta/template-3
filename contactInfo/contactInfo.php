<?php
//include './commonFunctions/functions.php';
if ($dataToShow == 'templateStockData') {
  $title = 'Request a call back right now ?';
  $description = 'Mauris ut dapibus velit cras interdum nisl ac urna tempor mollis.';
  $buttonText = 'Contact Us';
  $link = 'contact.php';
  $backgroundColor = '#a4c639';
} else{
  $title = $templateData[$x]['data']['title']['text'];
  $description = $templateData[$x]['data']['description']['text'];
  $buttonText = $templateData[$x]['data']['button']['text'];
  $backgroundColor = $templateData[$x]['data']['styles']['backgroundColor'];

  // if($templateData[$x]['data']['button']['linkType'] == 'default'){
  //     $link = 'contact'.$parameters;
  //   } else if($templateData[$x]['data']['button']['linkType'] == 'blog') {
  //     $link = $blogLink;
  //   } else if ($templateData[$x]['data']['button']['linkType'] == 'other'){
  //     $link = $templateData[$x]['data']['button']['link'];
  //   } else {
  //     $link = $templateData[$x]['data']['button']['linkType'].$parameters;
  //   }
  $link = getBlogLink($templateData[$x]['data']['button']['linkType'], $templateData[$x]['data']['button']['link'], $parameters);
  //$link = 'ds';
}

if($activeComponent == 'contactInfo'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}
?>
<div class="request-form-above-space">
</div>
<div class="request-form"  id="contactInfo<?php echo $x; ?>" style="<?php echo $showBorder ?> background-color: <?php echo $backgroundColor ?>">
      <div class="container">
        <div class="row">
          
          <div class="col-md-8">
            <h4><?php echo $title; ?></h4>
            <span><?php echo $description; ?></span>
          </div>
          <div class="col-md-4">
<?php
if($templateData[$x]['data']['button']['targetBlank']){
        echo '<a href='.$link.' class="border-button" target="_blank">'.$buttonText.'</a>';
      } else {
        echo '<a href='.$link.' class="border-button">'.$buttonText.'</a>';
      }
?>
            
          </div>
        
        </div>
      </div>
    </div>



