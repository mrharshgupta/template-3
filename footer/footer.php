<?php
if($activeComponent == 'footer'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}

if($templateData[$x]['data']['styles']['backgroundColor']){
  $backgroundColor = $templateData[$x]['data']['styles']['backgroundColor'];
} else {
  $backgroundColor = '#232323';
}

if($footerIndex){
  $x = $footerIndex;
} else {
  $x = $x;
}
//Whatsapp number
if(isset($storeInformation['phoneNumber'])){
  //file_exists('./environment.php')){
		$whatsappUrl = 'https://api.whatsapp.com/send?phone='.$storeInformation['phoneNumber'];
}else {
		$whatsappUrl = '#';
	}
?>

<?php
if ($dataToShow == 'templateStockData') {
echo '<footer id="footer'.$x.'" style="'.$showBorder.'">
      <div class="container">
        <div class="row">
          <div class="col-md-3 footer-item">
            <h4>Finance Business</h4>
            <p>Vivamus tellus mi. Nulla ne cursus elit,vulputate. Sed ne cursus augue hasellus lacinia sapien vitae.</p>
            <ul class="social-icons">
              <li><a rel="nofollow" href="https://fb.com/templatemo" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fa fa-behance"></i></a></li>
            </ul>
          </div>
          <div class="col-md-3 footer-item">
            <h4>Useful Links</h4>
            <ul class="menu-list">
              <li><a href="#">Vivamus ut tellus mi</a></li>
              <li><a href="#">Nulla nec cursus elit</a></li>
              <li><a href="#">Vulputate sed nec</a></li>
              <li><a href="#">Cursus augue hasellus</a></li>
              <li><a href="#">Lacinia ac sapien</a></li>
            </ul>
          </div>
          <div class="col-md-3 footer-item">
            <h4>Additional Pages</h4>
            <ul class="menu-list">
              <li><a href="#">About Us</a></li>
              <li><a href="#">How We Work</a></li>
              <li><a href="#">Quick Support</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>
          </div>
          <div class="col-md-3 footer-item last-item">
            <h4>Contact Us</h4>
            <div class="contact-form">
              <form id="contact footer-contact" action="" method="post">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="name" type="text" class="form-control" id="name" placeholder="Full Name" required="">
                    </fieldset>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="email" type="text" class="form-control" id="email" pattern="[^ @]*@[^ @]*" placeholder="E-Mail Address" required="">
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <button type="submit" id="form-submit" class="filled-button">Send Message</button>
                    </fieldset>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </footer>';
} else {
    echo '<footer id="footer'.$x.'" style="'.$showBorder.' background-color: '.$backgroundColor.';">
      <div class="container">
        <div class="row">
          <div class="col-md-3 footer-item">
            <h4>'.$templateData[$x]['data']['information']['name'].'</h4>
            <p>'.$templateData[$x]['data']['information']['description'].'</p>';
        
          
            echo '<ul class="social-icons">';
  $count = count($templateData[$x]['data']['social_links']);
  if($templateData[$x]['data']['social_links'][0] != 'true'){
  for ($i = 0; $i < $count; $i++) {
    if($templateData[$x]['data']['social_links'][$i]['targetBlank']){
      echo "<li><a href=".$templateData[$x]['data']['social_links'][$i]['link']." target='_blank'>";  
    } else {
      echo "<li><a href=".$templateData[$x]['data']['social_links'][$i]['link'].">";  
    }
    

    if($templateData[$x]['data']['social_links'][$i]['logo'] != 'other'){
      echo "<i class='".$templateData[$x]['data']['social_links'][$i]['logo']."'></i>";
    } else {
      $rand = rand();
      if($templateData[$x]['data']['social_links'][$i]['logoName'] != ''){
    echo "<img src='".$imagesDirectory."/".$templateData[$x]['data']['social_links'][$i]['logoName']."?rand=".$rand."' style='width: 20px; height: 20px' >";
    } else {
      echo "<i class='fas fa-circle'></i>";  
    }

}
echo "</a></li>";  
}} echo '</ul>';

          echo '</div>
          <div class="col-md-3 footer-item">
            <h4>Useful Links</h4>';
            
            $count = count($templateData[$x]['data']['usefulLinks']);
            if($templateData[$x]['data']['usefulLinks'][0] != 'true'){
            
                echo '<ul class="menu-list">';
            for ($i = 0; $i < $count; $i++) {
                // if($templateData[$x]['data']['usefulLinks'][$i]['linkType'] == 'default'){
                //     $link = 'javascript:void(0)';
                //   } else if($templateData[$x]['data']['usefulLinks'][$i]['linkType'] == 'blog') {
                //     $link = $blogLink;
                //   } else if ($templateData[$x]['data']['usefulLinks'][$i]['linkType'] == 'other'){
                //     $link = $templateData[$x]['data']['usefulLinks'][$i]['link'];
                //   } else {
                //     $link = $templateData[$x]['data']['usefulLinks'][$i]['linkType'].$parameters;
                //   }
                $link = getBlogLink($templateData[$x]['data']['usefulLinks'][$i]['linkType'], $templateData[$x]['data']['usefulLinks'][$i]['link'], $parameters); 
                  echo '<li>';
                  if($templateData[$x]['data']['usefulLinks'][$i]['targetBlank']){
                    echo '<a href='.$link.' target="_blank">';
                  } else {
                    echo '<a href='.$link.'>';
                  }
                echo $templateData[$x]['data']['usefulLinks'][$i]['name'].'</a></li>';
                }
               }
            echo '</ul>';
            echo '</div>';
          
            echo '<div class="col-md-3 footer-item">
            <h4>Additional Pages</h4>';
           
            $count = count($templateData[$x]['data']['additionalPages']);
            if($templateData[$x]['data']['additionalPages'][0] != 'true'){
            echo '<ul class="menu-list">';
            for ($i = 0; $i < $count; $i++) {
                // if($templateData[$x]['data']['additionalPages'][$i]['linkType'] == 'default'){
                //     $link = 'javascript:void(0)';
                //   } else if($templateData[$x]['data']['additionalPages'][$i]['linkType'] == 'blog') {
                //     $link = $blogLink;
                //   } else if ($templateData[$x]['data']['additionalPages'][$i]['linkType'] == 'other'){
                //     $link = $templateData[$x]['data']['additionalPages'][$i]['link'];
                //   } else {
                //     $link = $templateData[$x]['data']['additionalPages'][$i]['linkType'].$parameters;
                //   }
                $link = getBlogLink($templateData[$x]['data']['additionalPages'][$i]['linkType'], $templateData[$x]['data']['additionalPages'][$i]['link'], $parameters); 
                  echo '<li>';
                  if($templateData[$x]['data']['additionalPages'][$i]['targetBlank']){
                    echo '<a href='.$link.' target="_blank">';
                  } else {
                    echo '<a href='.$link.'>';
                  }
                echo $templateData[$x]['data']['additionalPages'][$i]['name'].'</a></li>';
                }
               }
           
          
              echo '</ul>'; 
              echo '</div>';
          
          echo '<div class="col-md-3 footer-item last-item">
            <h4>Contact Us</h4>
            <div class="contact-form">
              <form id="contact footer-contact" action="" method="post">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="name" type="text" class="form-control" id="name" placeholder="Full Name" required="">
                    </fieldset>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <fieldset>
                      <input name="email" type="text" class="form-control" id="email" pattern="[^ @]*@[^ @]*" placeholder="E-Mail Address" required="">
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <button type="submit" id="form-submit" class="filled-button">Send Message</button>
                    </fieldset>
                  </div>';
                 
                  if(isset($_POST['submit'])){
                    $name=$_POST['name'];
                    $email=$_POST['email'];
                    $message=$_POST['message'];
                    $to = $templateData[$x]['data']['contactForm']['email'];
                    //$to = 'harshgupta.harry@gmail.com';
                    //$subject = $title;
                    $txt = "Full Name: ".$name." Email: ".$email." message: ".$message;
                    $headers = "From: cling@example.com" . "\r\n";
                    mail($to,"New Msg",$txt,$headers);
                  }
                 
                echo '</div>
              </form>
            </div>
          </div>';
        if($storeInformation['selectedPlan'] == 2){
		      echo '<div><a href="'.$whatsappUrl.'" target="_blank"><img src="./assets/images/whatsapp_logo.png" alt="whatsapp" class="whatsapp_chatbot"/></a></div>';
      }
      echo '</div></div>
    </footer>';
}
?>