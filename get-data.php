<?php
if(file_exists('./environment.php')){    
	//this files has required credentials like userTemplateId, userId, templateId
	include ('./environment.php');
} else{
	$userTemplateId = htmlspecialchars($_GET["userTemplateId"]);
	$userId = htmlspecialchars($_GET["userId"]);
}

if(file_exists('../../files/composer/db.php')){    
	//mysql connection file for applied template
	include ('../../files/composer/db.php');
}
if(file_exists('../../composer/db.php')){    
	//mysql connection file for template to be edited
	include ('../../composer/db.php');
}

if(file_exists('../../files/composer/firebase_db.php')){    
	//firebase connection file
	include ('../../files/composer/firebase_db.php');
	
	//template data fetch from firebase
	$data = $database->getReference('user_template_data/'.$userTemplateId)->getValue();
	$templateStockData = $database->getReference('form_data/1/userData')->getValue();
	$settings = $database->getReference('userSettings/data/'.$userId)->getValue();
	//blog data fetch from firebase
	$blogData = $database->getReference('user_blog_data/'.$userId)->getSnapshot()->getValue();
	   $blogLink = '../../files/user-blogs/blog-list.php/?userId='.$userId;
	   
}	
if(file_exists('../../composer/firebase_db.php')){ 
	
	//firebase connection file
	include ('../../composer/firebase_db.php');
	
	//template data fetch from firebase
	$data = $database->getReference('user_template_data/'.$userTemplateId)->getValue();
	$templateStockData = $database->getReference('form_data/1/userData')->getValue();
	$settings = $database->getReference('userSettings/data/'.$userId)->getValue();
	//blog data fetch from firebase
	$blogData = $database->getReference('user_blog_data/'.$userId)->getSnapshot()->getValue();
   	$blogLink = '../../user-blogs/blog-list.php/?userId='.$userId;
}

//for preview during editing
$submit = htmlspecialchars($_GET["submit"]);

//active component name to make border while editing
$activeComponent = htmlspecialchars($_GET["activeComponent"]);
if($submit == 'true'){
	$maintenanceMode = false;
} else {
$maintenanceMode = $settings['storeSettings']['general']['maintenanceMode']['enable'];
}

//to set data to be displayed based on preview on editing and live preview
if(($userTemplateId == undefined || $userTemplateId == null || $data == null) || (($submit == 'false' || $submit == null) && $data['saved'] == null)){
	//to show preview without applying
	$dataToShow = 'stockData';
	$templateData = $templateStockData;
	
	
} elseif($submit == 'true'){
	//to show preview while editing
	
	$dataToShow = 'draftedData';
	$templateData = $data['submit'];
	               
} else {
	//to show live preview
	
	$dataToShow = 'confirmedData';
	$templateData = $data['saved'];
	               
}

//set parameters to pass between the links to show data at time of preview on editing
if($dataToShow == 'draftedData'){
  $parameters = '?submit=true';
} else {
  $parameters = false;
}

//to set active border
if($activeComponent){
$borderStyle = "border: 5px solid #1976d2;";  
}

//image directories
if(file_exists('./environment.php')){
	if($dataToShow == 'draftedData'){
		$imagesDirectory = 'https://api.onitt.co/onitt/files/media-manager/'.$userId.'/submit';
	} else {
		$imagesDirectory = 'https://api.onitt.co/onitt/files/media-manager/'.$userId.'/saved';
	}
} else {
	if($dataToShow == 'draftedData'){
		//$imagesDirectory = 'https://api.onitt.co/onitt/media-manager/'.$userId.'/submit';
		$imagesDirectory = '../../media-manager/'.$userId.'/submit';
	} else {
		$imagesDirectory = '../../media-manager/'.$userId.'/saved';
	}
} 


$storeInformation = array("email"=>$settings['storeSettings']['general']['contactInformation']['email'],
 "homePageTitle"=>$settings['storeSettings']['general']['seoData']['homePageTitle'], 
 "homePageMetaDescription"=>$settings['storeSettings']['general']['seoData']['homePageMetaDescription'],
 "storeLogo"=>$settings['storeSettings']['general']['others']['logo'],
 "favicon"=>$settings['storeSettings']['general']['others']['favicon'],
 "showClientPhoneNumberField"=>$settings['storeSettings']['general']['contactUsData']['showClientPhoneNumberField'],
 "phoneNumber"=>$settings['storeSettings']['general']['contactInformation']['mobileNumber'],
);

$getUserDetails = "SELECT * FROM login WHERE id = '".$userId."'";
 	$mainResult  = $conn->query($getUserDetails);
 	if ($mainResult->num_rows > 0) {
		$row = $mainResult->fetch_assoc();
		 
		 $storeInformation['selectedPlan'] = $row['plan'];
		 
 	}




?>