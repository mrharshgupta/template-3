<?php 
include 'get-data.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <style>
      .navbar-logo {
        margin-top: -30px; height: 60px; width: 200px;
      }
      .social-icon: {width: 20px; height: 20px;}
      .request-form-above-space: {height: 50px;}
      /* MAP */
      .map-iframe { margin-top: 35px;}
      </style>
    <meta charset="utf-8">
    <?php 
      if($includeBaseTag){
        echo "<base href='/' />";
      }
    ?> 
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content=<?php echo $storeInformation["homePageMetaDescription"]; ?>>
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title><?php echo $storeInformation["homePageTitle"]; ?></title>

    <link rel="icon" href=<?php echo $storeInformation["favicon"]; ?> type="image/gif" sizes="16x16">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/extra-styles.css">
    <link rel="stylesheet" href="assets/css/templatemo-finance-business.css">
    <link rel="stylesheet" href="assets/css/owl.css">

<!--
Finance Business TemplateMo

https://templatemo.com/tm-545-finance-business
-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    



  </head>