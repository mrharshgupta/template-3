<?php
$menuItems = ['index', 'about', 'service', 'contact'];
$linkNames = ['home', 'about', 'service', 'contact'];
if ($dataToShow == 'templateStockData') {
  
  echo '<li class="nav-item active">
                <a class="nav-link" href="index">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about">About Us</a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link" href="services">Our Services</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact">Contact Us</a>
              </li>';
} else {
  $count = count($templateData[$x]['data']['menus']);
  if($templateData[$x]['data']['menus'][0] != 'true'){
  for ($i = 0; $i < $count; $i++) {
    $linkArray = explode("-", $templateData[$x]['data']['menus'][$i]['linkType']);
    $linkArray = explode("-", $templateData[$x]['data']['menus'][$i]['linkType']);
    if($templateData[$x]['data']['menus'][$i]['linkType'] == 'default'){
      $link = $menuItems[$i].$parameters;
    } else if($templateData[$x]['data']['menus'][$i]['linkType'] == 'blog') {
      $link = 'blog-list'.$parameters;
    } else if ($templateData[$x]['data']['menus'][$i]['linkType'] == 'other'){
      $link = $templateData[$x]['data']['menus'][$i]['link'];
    } else if($templateData[$x]['data']['menus'][$i]['linkType'] == 'about' || $templateData[$x]['data']['menus'][$i]['linkType'] == 'contact' || $templateData[$x]['data']['menus'][$i]['linkType'] == 'service'){
      $link = $templateData[$x]['data']['menus'][$i]['linkType'];
    } else if($templateData[$x]['data']['menus'][$i]['linkType'] == 'home'){
      $link = 'index';
    } else if($linkArray[0] == 'blog'){
      $parameters ? $link = 'blog'.$parameters.'&id='.$templateData[$x]['data']['menus'][$i]['linkType'] : $link = 'blog/'.$templateData[$x]['data']['menus'][$i]['linkType'];
    } else if($linkArray[0] == 'webpage') {
      $parameters ? $link = 'webpage'.$parameters.'&id='.$templateData[$x]['data']['menus'][$i]['linkType'] : $link = 'webpage/'.$templateData[$x]['data']['menus'][$i]['linkType'];
    }
    if($linkNames[$i] == $activeLinkName){
      echo '<li class="nav-item active">';
      
      if($templateData[$x]['data']['menus'][$i]['targetBlank']){
        echo '<a class="nav-link" href='.$link.' target="_blank">';
      } else {
        echo '<a class="nav-link" href='.$link.'>';
      }
      echo $templateData[$x]['data']['menus'][$i]['name'].'</a>
        </li>';
     } else {
       echo '<li class="nav-item">';

       if($templateData[$x]['data']['menus'][$i]['targetBlank']){
    echo '<a class="nav-link" href='.$link.' target="_blank">';
  } else {
    echo '<a class="nav-link" href='.$link.'>';
  }
    echo $templateData[$x]['data']['menus'][$i]['name'].'</a>
              </li>';
     }
  }}
  
}?>
