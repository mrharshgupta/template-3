<?php
//include './commonFunctions/functions.php';
if($activeComponent == 'imageWithText'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}
if($templateData[$x]['data']['styles']['backgroundColor']){
                $backgroundColor = $templateData[$x]['data']['styles']['backgroundColor'];
              } else {
                $backgroundColor = null;
              }  
?>

<?php
if ($dataToShow == 'templateStockData') {
	echo '<div class="more-info" id="imageWithText'.$x.'" style="'.$showBorder.'">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="more-info-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="left-image">
                    <img src="assets/images/more-info.jpg" alt="">
                  </div>
                </div>
                <div class="col-md-6 align-self-center">
                  <div class="right-content">
                    <span>Who we are</span>
                    <h2>Get to know about <em>our company</em></h2>
                    <p>Curabitur pulvinar sem a leo tempus facilisis. Sed non sagittis neque. Nulla conse quat tellus nibh, id molestie felis sagittis ut. Nam ullamcorper tempus ipsum in cursus<br><br>Praes end at dictum metus. Morbi id hendrerit lectus, nec dapibus ex. Etiam ipsum quam, luctus eu egestas eget, tincidunt</p>
                    <a href="#" class="filled-button">Read More</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
} else{
	echo '<div class="more-info" id="imageWithText'.$x.'" style="'.$showBorder.'">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="more-info-content">
              <div class="row" >
                <div class="col-md-6">
                  <div class="left-image" >';
                    if($templateData[$x]['data']['backgroundImage'] != ''){    
                      $rand = rand();
                      echo '<img src="'.$imagesDirectory.'/'.$templateData[$x]['data']['backgroundImage'].'?rand='.$rand.' alt="">';
                    } else {
                        echo '<img src="assets/images/more-info.jpg" alt="">';
                      }
            echo '</div>
                </div>
                <div class="col-md-6 align-self-center" >
                  <div class="right-content">
                    <span>'.$templateData[$x]['data']['heading']['text'].'</span>
                    <h2>'.$templateData[$x]['data']['title']['text'].'</h2>';
                    echo '<p>';
                    //for ($x = 0; $x < $formData['imageWithText']['description']; $x++) {
                      echo $templateData[$x]['data']['description']['text'];
                      echo '<br><br>';
                    //}
                    echo '</p>';
                    
                    // if($templateData[$x]['data']['button']['linkType'] == 'default'){
                    //   $link = 'javascript:void(0)';
                    // } else if($templateData[$x]['data']['button']['linkType'] == 'blog') {
                    //   //$link = $blogLink;
                    //   $link = 'blog-list'.$parameters;
                    // } else if ($templateData[$x]['data']['button']['linkType'] == 'other'){
                    //   $link = $templateData[$x]['data']['button']['link'];
                    // } else if($templateData[$x]['data']['button']['linkType'] == 'about' || $templateData[$x]['data']['button']['linkType'] == 'contact' || $templateData[$x]['data']['button']['linkType'] == 'service'){
                    //   $link = $templateData[$x]['data']['button']['linkType'];
                    // } else {
                    //   $link = 'webpage?id='.$templateData[$x]['data']['button']['linkType'].$parameters;
                    // }
                    $link = getBlogLink($templateData[$x]['data']['button']['linkType'], $templateData[$x]['data']['button']['link'], $parameters);
                    //$link = 'dsdsc';
                    if($templateData[$x]['data']['button']['targetBlank']){
                    echo '<a href="'.$link.'" class="filled-button" target="_blank">';
                    } else {
                      echo '<a href="'.$link.'" class="filled-button">';
                    }
                    echo $templateData[$x]['data']['button']['text'].'</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
}
?>