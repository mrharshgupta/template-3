<?php 
include 'header.php';
include './commonFunctions/functions.php';
?>
<body>

<?php 
$templateData = $templateData['home'];
$activeLinkName = 'home';

$x = 0;
if(!$maintenanceMode){
  $count = count($templateStockData);
include './header/header.php';
}

function includeFile($file, 
                     $templateData, 
                     $dataToShow, 
                     $parameter, 
                     $borderStyle, 
                     $imagesDirectory, 
                     $userId, 
                     $activeComponent, 
                     $blogLink, 
                     $x,
                     $storeInformation) {
    $templateData = $templateData;
    $dataToShow = $dataToShow; 
    $parameters = $parameters; 
    $borderStyle = $borderStyle; 
    $imagesDirectory = $imagesDirectory; 
    $userId = $userId; 
    $activeComponent = $activeComponent; 
    $blogLink = $blogLink;
    $storeInformation = $storeInformation;

    
    include($file);
}

if(!$maintenanceMode){
  
if($dataToShow == 'templateStockData'){
  
 include './carousel/carousel.php';

 include './contactInfo/contactInfo.php';

 include './services/services.php';

 include './stats/stats.php';

 include './imageWithText/imageWithText.php';

 include './photoGallery/photoGallery.php';  

 include './testimonials/testimonials.php';

 include './contactForm/contactForm.php';

 include './partners/partners.php';  

 //include './footer/footer.php';
 } else {
 
 
$count = count($templateData);

 for($x = 1; $x < $count - 1; $x++ ){
 //echo "dsc".$templateData[$x]['sectionInfo']['type'];
  if($templateData[$x]['sectionSettings']['show'] == 'true'){
    includeFile('./'.$templateData[$x]['sectionInfo']['type'].'/'.$templateData[$x]['sectionInfo']['type'].'.php', 
                $templateData, 
                $dataToShow, 
                $parameters, 
                $borderStyle, 
                $imagesDirectory, 
                $userId, 
                $activeComponent, 
                $blogLink,
                $x,
                $storeInformation
              );
  }
 }
}
} else {
include './maintenance.html';
// echo '<h1>Under Maintenance</h1>';
}

if(!$maintenanceMode){
  include './footer/footer.php';
}
?>

    <?php
    if(!$maintenanceMode){
    echo '<div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p>Copyright &copy; 2020 Financial Business Co., Ltd.
            
            - Design: <a rel="nofollow noopener" href="https://templatemo.com" target="_blank">TemplateMo</a></p>
          </div>
        </div>
      </div>
    </div>';
    } ?>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Additional Scripts -->
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/accordions.js"></script>
    
    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>
<script type="text/javascript">

$(document).ready(function () {
  var activeComponent = '<?php echo $activeComponent; ?>';
  
  //$('#' + activeComponent).animate({scrollTop:0}, '500', 'swing');
  console.log(activeComponent, $('#' + activeComponent).offset(), 'activeComponentdsc')
    $('html, body').animate({
        scrollTop: $('#' + activeComponent).position().top - 100
    }, 'slow');

  //   $('#myButton').click(function(event) {
  //    event.preventDefault();
  //  $.scrollTo($('#myDiv'), 1000);
});



</script>
  </body>
</html>