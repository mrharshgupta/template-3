<?php
if($activeComponent == 'photoGallery'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}
  

if($templateData[$x]['data']['styles']['backgroundColor']){
                $backgroundColor = $templateData[$x]['data']['styles']['backgroundColor'];
              } else {
                $backgroundColor = null;
              }  
?>
<div style="font-family: Verdana, sans-serif; margin: 0;">
<style>
body {
  font-family: Verdana, sans-serif;
  margin: 0;
}

* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  top: 50%;
  left: 50%;
  /* bring your own prefixes */
  /* transform: translate(-50%, -50%); */
  z-index: 9999999;
  /* padding-top: 100px; */
  left: 0;
  top: 0;
  /* width: 70%;
  height: 70%; */
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  /* position: relative; */
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 70%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 25px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}
.imageStyle {
  width:100%; 
  margin-top: 20px; 
  margin-bottom: 20px;
  max-height: 200px;
  max-width: 300px;
  object-fit: 'contain'
}
/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
  
<div class="photoGallery" id="photoGallery<?php echo $x; ?>" style="<?php echo $showBorder; ?>" style="margin-top: 50px; padding-right: 20px; padding-left: 20px">
<?php echo '<div class="col-md-12">
            <div class="section-heading">
            <h2>'.$templateData[$x]['data']['heading']['text'].'</h2>
            <span>'.$templateData[$x]['data']['description']['text'].'</span>
          </div>
        </div>'; ?> 
<div class="row" style="padding-horizontal: 30px">
<?php 

$count = count($templateData[$x]['data']['photos']);
              if($templateData[$x]['data']['photos'][0] != 'true'){  
              for ($i = 0; $i < $count; $i++) {

echo '<div class="column">';
if($templateData[$x]['data']['photos'][$i]['backgroundImage'] != ''){    
  $rand = rand();
  $currentSilde = $i+1;
  echo '<img src="'.$imagesDirectory.'/'.$templateData[$x]['data']['photos'][$i]['backgroundImage'].'?rand='.$rand.'" style="" onclick="openModal();currentSlide('.$currentSilde.')" class="imageStyle hover-shadow cursor">';
 } else {
  echo '<img src="assets/images/client-01.png" alt="" class="imageStyle hover-shadow cursor">';
}
echo '</div>';
              }}
?>

 
  
</div>

<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content" style="background-color: black;">


  <?php 

$count = count($templateData[$x]['data']['photos']);
              if($templateData[$x]['data']['photos'][0] != 'true'){  
              for ($i = 0; $i < $count; $i++) {
$currentImageIndex = $i+1;
echo '<div class="mySlides"><div class="numbertext">';
echo $currentImageIndex." / ".$count;
echo'</div>';
if($templateData[$x]['data']['photos'][$i]['backgroundImage'] != ''){    
  $rand = rand();
echo '<img src="'.$imagesDirectory.'/'.$templateData[$x]['data']['photos'][$i]['backgroundImage'].'?rand='.$rand.'" style="object-fit: contain; max-width:100%;">';
} else {
  echo '<img src="assets/images/client-01.png" style="width:100%">';
}
echo '</div>';
              }}
?>


    <!-- <div class="mySlides">
      <div class="numbertext">1 / 2</div>
      <img src="https://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(145).jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 2</div>
      <img src="https://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(145).jpg" style="width:100%">
    </div> -->

    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

  
   
  </div>
</div>
</div>
<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i]?.className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1]?.className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
</div>