<?php

if($activeComponent == 'services'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}

if ($dataToShow == 'templateStockData') {
  echo '<div class="services" id="services'.$x.'" style="'.$showBorder.'">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>Financial <em>Services</em></h2>
              <span>Aliquam id urna imperdiet libero mollis hendrerit</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-item">
              <img src="assets/images/service_01.jpg" alt="">
              <div class="down-content">
                <h4>Digital Currency</h4>
                <p>Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.</p>
                <a href="" class="filled-button">Read More</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-item">
              <img src="assets/images/service_02.jpg" alt="">
              <div class="down-content">
                <h4>Digital Currency</h4>
                <p>Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.</p>
                <a href="" class="filled-button">Read More</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-item">
              <img src="assets/images/service_03.jpg" alt="">
              <div class="down-content">
                <h4>Digital Currency</h4>
                <p>Sed tincidunt dictum lobortis. Aenean tempus diam vel augue luctus dignissim. Nunc ornare leo tortor.</p>
                <a href="" class="filled-button">Read More</a>
              </div>
            </div>
          </div>
        </div>  </div>
    </div>';  

} else{
  echo '<div class="services" id="services'.$x.'" style="'.$showBorder.'">
      <div class="container">
      <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>'.$templateData[$x]['data']['title']['text'].'</h2>
              <span>'.$templateData[$x]['data']['description']['text'].'</span>
            </div>
            
          </div>';
          
          $count = count($templateData[$x]['data']['services']);
          if($templateData[$x]['data']['services'][0] != 'true'){
  for ($i = 0; $i < $count; $i++) {
    $link = getBlogLink($templateData[$x]['data']['services'][$i]['button']['linkType'], $templateData[$x]['data']['services'][$i]['button']['link'], $parameters);

    echo '<div class="col-md-4">
            <div class="service-item">';
    if($templateData[$x]['data']['services'][$i]['backgroundImage'] != ''){   
      $rand = rand();
      echo '<img src="'.$imagesDirectory.'/'.$templateData[$x]['data']['services'][$i]['backgroundImage'].'?rand='.$rand.' alt="" style="height: 200px">';
     } else {
      echo '<img src="assets/images/service_02.jpg" alt="">';
    }

              if($templateData[$x]['data']['services'][$i]['styles']['backgroundColor']){
                $serviceBackgroundColor = $templateData[$x]['data']['services'][$i]['styles']['backgroundColor'];
              } else {
                $serviceBackgroundColor = null;
              }
              if($templateData[$x]['data']['services'][$i]['button']['styles']['backgroundColor']){
                $serviceButtonBackgroundColor = $templateData[$x]['data']['services'][$i]['button']['styles']['backgroundColor'];
              } else {
                $serviceButtonBackgroundColor = null;
              }
              echo '<div class="down-content" style="height: 300px; background-color: '.$serviceBackgroundColor.';">
                <h4>'.$templateData[$x]['data']['services'][$i]['title']['text'].'</h4>
                <p>'.$templateData[$x]['data']['services'][$i]['description']['text'].'</p>';
                
                 if($templateData[$x]['data']['services'][$i]['button']['targetBlank']){
                   echo '<a href='.$link.' target="_blank" class="filled-button" style="background-color: '.$serviceButtonBackgroundColor.';">';
                 } else {
                   echo '<a href='.$link.' class="filled-button" style="background-color: '.$serviceButtonBackgroundColor.';">';
                 }
                echo $templateData[$x]['data']['services'][$i]['button']['text'].'</a>
              </div>
            </div>
          </div>';
  
  }  }
  echo '</div>  </div>
    </div>';


}

?>