<?php
if($activeComponent == 'stats'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}
if($templateData[$x]['data']['button']['styles']['backgroundColor']){
                $buttonColor = $templateData[$x]['data']['button']['styles']['backgroundColor'];
              } else {
                $buttonColor = null;
              }  
?>


<?php
if ($dataToShow == 'templateStockData') {
  echo '<div class="fun-facts" id="statistics'.$x.'" style="'.$showBorder.'">
      <div class="container">
        <div class="row"><div class="col-md-6">
            <div class="left-content">
              <span>Lorem ipsum dolor sit amet</span>
              <h2>Our solutions for your <em>business growth</em></h2>
              <p>Pellentesque ultrices at turpis in vestibulum. Aenean pretium elit nec congue elementum. Nulla luctus laoreet porta. Maecenas at nisi tempus, porta metus vitae, faucibus augue. 
              <br><br>Fusce et venenatis ex. Quisque varius, velit quis dictum sagittis, odio velit molestie nunc, ut posuere ante tortor ut neque.</p>
              <a href="" class="filled-button">Read More</a>
            </div>
          </div>
          <div class="col-md-6 align-self-center">
            <div class="row">
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">945</div>
                  <div class="count-title">Work Hours</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">1280</div>
                  <div class="count-title">Great Reviews</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">578</div>
                  <div class="count-title">Projects Done</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">26</div>
                  <div class="count-title">Awards Won</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
} else{

  if($templateData[$x]['data']['backgroundImage'] != ''){    
      $rand = rand();
       echo '<div class="fun-facts" id="statistics'.$x.'" style="'.$showBorder.'background-image: url('.$imagesDirectory.'/'.$templateData[$x]['data']['backgroundImage'].'?rand='.$rand.');">';
     } else {
       echo '<div class="fun-facts" id="statistics'.$x.'" style="'.$showBorder.'">';
    }

  echo '<div class="container">
        <div class="row"><div class="col-md-6">
          <div class="left-content">
            <span>'.$templateData[$x]['data']['heading']['text'].'</span>
            <h2>'.$templateData[$x]['data']['title']['text'].'</h2>';
            echo '<p>'; 
            
            $count = count($templateData[$x]['data']['description']);
            if($templateData[$x]['data']['description'][0] != 'true'){
            for ($i = 0; $i < $count; $i++) {
              echo $templateData[$x]['data']['description'][$i]['text'].'
                <br><br>';
            }}
            echo '</p>';
    // if($templateData[$x]['data']['button']['linkType'] == 'default'){
    //   $link = 'javascript:void(0)';
    // } else if($templateData[$x]['data']['button']['linkType'] == 'blog') {
    //   $link = $blogLink;
    // } else if ($templateData[$x]['data']['button']['linkType'] == 'other'){
    //   $link = $templateData[$x]['data']['button']['link'];
    // } else {
    //   $link = $templateData[$x]['data']['button']['linkType'].$parameters;
    // }  
    $link = getBlogLink($templateData[$x]['data']['button']['linkType'], $templateData[$x]['data']['button']['link'], $parameters); 
    if($templateData[$x]['data']['button']['targetBlank']){
      
      echo '<a href="'.$link.'" target="_blank" class="filled-button" style="background-color: '.$buttonColor.';">';
    } else {
      echo '<a href="'.$link.'" class="filled-button" style="background-color: '.$buttonColor.';">';
      
    }        
    echo $templateData[$x]['data']['button']['text'].'</a>
            </div>
          </div>
          <div class="col-md-6 align-self-center">
            <div class="row">';
             
            $count = count($templateData[$x]['data']["stats"]);
            if($templateData[$x]['data']['stats'][0] != 'true'){
            for ($i = 0; $i < $count; $i++) {

              echo '<div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">'.$templateData[$x]['data']['stats'][$i]['statNumber'].'</div>
                  <div class="count-title">'.$templateData[$x]['data']['stats'][$i]['statName'].'</div>
                </div>
              </div>';
              }
            }
            echo '</div>
          </div>
        </div>
      </div>
    </div>';
}
?>

