<?php

if($activeComponent == 'team'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}

  echo '<div class="services" id="team'.$x.'" style="'.$showBorder.'">'; ?>
  <style>
  .memberImage {
      height: 200px
  }
  /* .memberCard {
      height: 300px
  } */
  </style>
      <?php echo '<div class="container">
      <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>'.$templateData[$x]['data']['title']['text'].'</h2>
              <span>'.$templateData[$x]['data']['description']['text'].'</span>
            </div>
          </div>';
          $count = count($templateData[$x]['data']['team']);
          if($templateData[$x]['data']['team'][0] != 'true'){
  for ($i = 0; $i < $count; $i++) {
    

    echo '<div class="col-md-3">
            <div class="memberCard service-item">';
    if($templateData[$x]['data']['team'][$i]['backgroundImage'] != ''){    
      $rand = rand();
      echo '<img class="memberImage" src="'.$imagesDirectory.'/'.$templateData[$x]['data']['team'][$i]['backgroundImage'].'?rand='.$rand.' alt="">';
     } else {
      echo '<img class="memberImage" src="assets/images/avatar.png" alt="">';
    }

              if($templateData[$x]['data']['team'][$i]['styles']['backgroundColor']){
                $serviceBackgroundColor = $templateData[$x]['data']['team'][$i]['styles']['backgroundColor'];
              } else {
                $serviceBackgroundColor = null;
              }
             
              echo '<div class="down-content" style="height: 300px; background-color: '.$serviceBackgroundColor.';">
                <h4>'.$templateData[$x]['data']['team'][$i]['name']['text'].'</h4>
                <h6>'.$templateData[$x]['data']['team'][$i]['position']['text'].'</h6>
                <p>'.$templateData[$x]['data']['team'][$i]['description']['text'].'</p>';
                
               
                echo '</div>
            </div>
          </div>';
  
  }  }
  echo '</div>  </div>
    </div>';




?>