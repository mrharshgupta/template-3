<?php
if($activeComponent == 'testimonials'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}

if($templateData[$x]['data']['styles']['backgroundColor']){
                $backgroundColor = $templateData[$x]['data']['styles']['backgroundColor'];
              } else {
                $backgroundColor = null;
              }  
?>

<?php
if ($dataToShow == 'templateStockData') {
echo '<div class="testimonials" id="testimonials'.$x.'" style="'.$showBorder.'">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>What they say <em>about us</em></h2>
              <span>testimonials from our greatest clients</span>
            </div>
          </div>
          <div class="col-md-12">
            <div class="owl-testimonials owl-carousel">
              
              <div class="testimonial-item">
                <div class="inner-content">
                  <h4>George Walker</h4>
                  <span>Chief Financial Analyst</span>
                  <p>"Nulla ullamcorper, ipsum vel condimentum congue, mi odio vehicula tellus, sit amet malesuada justo sem sit amet quam. Pellentesque in sagittis lacus."</p>
                </div>
                <img src="http://placehold.it/60x60" alt="">
              </div>
              
              <div class="testimonial-item">
                <div class="inner-content">
                  <h4>John Smith</h4>
                  <span>Market Specialist</span>
                  <p>"In eget leo ante. Sed nibh leo, laoreet accumsan euismod quis, scelerisque a nunc. Mauris accumsan, arcu id ornare malesuada, est nulla luctus nisi."</p>
                </div>
                <img src="http://placehold.it/60x60" alt="">
              </div>
              
              <div class="testimonial-item">
                <div class="inner-content">
                  <h4>David Wood</h4>
                  <span>Chief Accountant</span>
                  <p>"Ut ultricies maximus turpis, in sollicitudin ligula posuere vel. Donec finibus maximus neque, vitae egestas quam imperdiet nec. Proin nec mauris eu tortor consectetur tristique."</p>
                </div>
                <img src="http://placehold.it/60x60" alt="">
              </div>
              
              <div class="testimonial-item">
                <div class="inner-content">
                  <h4>Andrew Boom</h4>
                  <span>Marketing Head</span>
                  <p>"Curabitur sollicitudin, tortor at suscipit volutpat, nisi arcu aliquet dui, vitae semper sem turpis quis libero. Quisque vulputate lacinia nisl ac lobortis."</p>
                </div>
                <img src="http://placehold.it/60x60" alt="">
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>';
} else {
  echo '<div class="testimonials" id="testimonials'.$x.'" style="'.$showBorder.' background-color: '.$backgroundColor.';">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>'.$templateData[$x]['data']['title']['text'].'</h2>
              <span>'.$templateData[$x]['data']['description']['text'].'</span>
            </div>
          </div>
          <div class="col-md-12">
            <div class="owl-testimonials owl-carousel">';
              
$count = count($templateData[$x]['data']['testimonials']); 
if($templateData[$x]['data']['testimonials'][0] != 'true'){             
for ($i = 0; $i < $count; $i++) {

    if($templateData[$x]['data']['testimonials'][$i]['styles']['backgroundColor']){
                $testimonialBackgroundColor = $templateData[$x]['data']['testimonials'][$i]['styles']['backgroundColor'];
              } else {
                $testimonialBackgroundColor = null;
              }  

              echo '<div class="testimonial-item">
                <div class="inner-content"  style="background-color: '.$testimonialBackgroundColor.'">
                  <h4>'.$templateData[$x]['data']['testimonials'][$i]['name'].'</h4>
                  <span>'.$templateData[$x]['data']['testimonials'][$i]['position'].'</span>
                  <p>'.$templateData[$x]['data']['testimonials'][$i]['review'].'</p>
                </div>';
                
if($templateData[$x]['data']['testimonials'][$i]['backgroundImage'] != ''){    
      $rand = rand();
      echo '<img src="'.$imagesDirectory.'/'.$templateData[$x]['data']['testimonials'][$i]['backgroundImage'].'?rand='.$rand.' alt="" style="height: 60px; width: 60px">';
     } else {
      echo '<img src="http://placehold.it/60x60" alt="">';
    }


                echo '</div>';
            }}
              
            echo '</div>
          </div>
        </div>
      </div>
    </div>';
}

?>