<?php
if($activeComponent == 'textWithBackgroundImage'.$x){
  $showBorder = $borderStyle;  
} else{
  $showBorder = null;    
}
if($templateData[$x]['data']['styles']['backgroundColor']){
                $backgroundColor = $templateData[$x]['data']['styles']['backgroundColor'];
              } else {
                $backgroundColor = null;
              }  
?>

<?php
if ($dataToShow == 'templateStockData') {
	echo '<div class="page-heading header-text" id="textWithBackgroundImage'.$x.'" style="'.$showBorder.'">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>About Us</h1>
          <span>We have over 20 years of experience</span>
        </div>
      </div>
    </div>
  </div>';
} else{
    if($templateData[$x]['data']['styles']['background']['image'] != ''){    
        $rand = rand();
        echo'<div class="page-heading header-text" id="textWithBackgroundImage'.$x.'" style="background-image: url('.$imagesDirectory.'/'.$templateData[$x]["data"]["styles"]["background"]["image"].'?rand='.$rand.');">';
        
       } else {
        echo'<div class="page-heading header-text" id="textWithBackgroundImage'.$x.'" style="'.$showBorder.'">';
      }
    
    $rand = rand();
	echo '<div class="container">
      <div class="row">
      
        <div class="col-md-12">
          <h1>'.$templateData[$x]['data']['heading']['text'].'</h1>
          
          <span>'.$templateData[$x]['data']['description']['text'].'</span>
        </div>
      </div>
    </div>
  </div>';
}
?>